/*
 * generated by Xtext 2.14.0
 */
grammar InternalMyDsl;

options {
	superClass=AbstractInternalContentAssistParser;
}

@lexer::header {
package com.javadude.rube.state.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package com.javadude.rube.state.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import com.javadude.rube.state.services.MyDslGrammarAccess;

}
@parser::members {
	private MyDslGrammarAccess grammarAccess;

	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}

	@Override
	protected Grammar getGrammar() {
		return grammarAccess.getGrammar();
	}

	@Override
	protected String getValueForTokenName(String tokenName) {
		return tokenName;
	}
}

// Entry rule entryRulePiece
entryRulePiece
:
{ before(grammarAccess.getPieceRule()); }
	 rulePiece
{ after(grammarAccess.getPieceRule()); } 
	 EOF 
;

// Rule Piece
rulePiece 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getPieceAccess().getGroup()); }
		(rule__Piece__Group__0)
		{ after(grammarAccess.getPieceAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleImage
entryRuleImage
:
{ before(grammarAccess.getImageRule()); }
	 ruleImage
{ after(grammarAccess.getImageRule()); } 
	 EOF 
;

// Rule Image
ruleImage 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getImageAccess().getGroup()); }
		(rule__Image__Group__0)
		{ after(grammarAccess.getImageAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleStateMachine
entryRuleStateMachine
:
{ before(grammarAccess.getStateMachineRule()); }
	 ruleStateMachine
{ after(grammarAccess.getStateMachineRule()); } 
	 EOF 
;

// Rule StateMachine
ruleStateMachine 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getStateMachineAccess().getGroup()); }
		(rule__StateMachine__Group__0)
		{ after(grammarAccess.getStateMachineAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleState
entryRuleState
:
{ before(grammarAccess.getStateRule()); }
	 ruleState
{ after(grammarAccess.getStateRule()); } 
	 EOF 
;

// Rule State
ruleState 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getStateAccess().getGroup()); }
		(rule__State__Group__0)
		{ after(grammarAccess.getStateAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

// Entry rule entryRuleTransition
entryRuleTransition
:
{ before(grammarAccess.getTransitionRule()); }
	 ruleTransition
{ after(grammarAccess.getTransitionRule()); } 
	 EOF 
;

// Rule Transition
ruleTransition 
	@init {
		int stackSize = keepStackSize();
	}
	:
	(
		{ before(grammarAccess.getTransitionAccess().getGroup()); }
		(rule__Transition__Group__0)
		{ after(grammarAccess.getTransitionAccess().getGroup()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Alternatives_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getStateMachineAccess().getImagesAssignment_0_0()); }
		(rule__StateMachine__ImagesAssignment_0_0)
		{ after(grammarAccess.getStateMachineAccess().getImagesAssignment_0_0()); }
	)
	|
	(
		{ before(grammarAccess.getStateMachineAccess().getStatesAssignment_0_1()); }
		(rule__StateMachine__StatesAssignment_0_1)
		{ after(grammarAccess.getStateMachineAccess().getStatesAssignment_0_1()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Piece__Group__0__Impl
	rule__Piece__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPieceAccess().getPieceKeyword_0()); }
	'piece'
	{ after(grammarAccess.getPieceAccess().getPieceKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Piece__Group__1__Impl
	rule__Piece__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPieceAccess().getNameAssignment_1()); }
	(rule__Piece__NameAssignment_1)
	{ after(grammarAccess.getPieceAccess().getNameAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Piece__Group__2__Impl
	rule__Piece__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPieceAccess().getStuffKeyword_2()); }
	'stuff'
	{ after(grammarAccess.getPieceAccess().getStuffKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Piece__Group__3__Impl
	rule__Piece__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPieceAccess().getThatKeyword_3()); }
	'that'
	{ after(grammarAccess.getPieceAccess().getThatKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Piece__Group__4__Impl
	rule__Piece__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPieceAccess().getCanKeyword_4()); }
	'can'
	{ after(grammarAccess.getPieceAccess().getCanKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__5
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Piece__Group__5__Impl
	rule__Piece__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__5__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPieceAccess().getHappenKeyword_5()); }
	'happen'
	{ after(grammarAccess.getPieceAccess().getHappenKeyword_5()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__6
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Piece__Group__6__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__Group__6__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getPieceAccess().getStateMachineAssignment_6()); }
	(rule__Piece__StateMachineAssignment_6)
	{ after(grammarAccess.getPieceAccess().getStateMachineAssignment_6()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Image__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Image__Group__0__Impl
	rule__Image__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImageAccess().getShowKeyword_0()); }
	'show'
	{ after(grammarAccess.getImageAccess().getShowKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Image__Group__1__Impl
	rule__Image__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImageAccess().getImageKeyword_1()); }
	'image'
	{ after(grammarAccess.getImageAccess().getImageKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Image__Group__2__Impl
	rule__Image__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImageAccess().getFileAssignment_2()); }
	(rule__Image__FileAssignment_2)
	{ after(grammarAccess.getImageAccess().getFileAssignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Image__Group__3__Impl
	rule__Image__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImageAccess().getForKeyword_3()); }
	'for'
	{ after(grammarAccess.getImageAccess().getForKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Image__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getImageAccess().getStateAssignment_4()); }
	(rule__Image__StateAssignment_4)
	{ after(grammarAccess.getImageAccess().getStateAssignment_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__StateMachine__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__StateMachine__Group__0__Impl
	rule__StateMachine__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStateMachineAccess().getAlternatives_0()); }
	(rule__StateMachine__Alternatives_0)*
	{ after(grammarAccess.getStateMachineAccess().getAlternatives_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__StateMachine__Group__1__Impl
	rule__StateMachine__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStateMachineAccess().getStartKeyword_1()); }
	'start'
	{ after(grammarAccess.getStateMachineAccess().getStartKeyword_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__StateMachine__Group__2__Impl
	rule__StateMachine__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStateMachineAccess().getHereKeyword_2()); }
	'here'
	{ after(grammarAccess.getStateMachineAccess().getHereKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__StateMachine__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStateMachineAccess().getInitialStateAssignment_3()); }
	(rule__StateMachine__InitialStateAssignment_3)
	{ after(grammarAccess.getStateMachineAccess().getInitialStateAssignment_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__State__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__State__Group__0__Impl
	rule__State__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__State__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStateAccess().getStatusKeyword_0()); }
	'status'
	{ after(grammarAccess.getStateAccess().getStatusKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__State__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__State__Group__1__Impl
	rule__State__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__State__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStateAccess().getNameAssignment_1()); }
	(rule__State__NameAssignment_1)
	{ after(grammarAccess.getStateAccess().getNameAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__State__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__State__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__State__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getStateAccess().getTransitionsAssignment_2()); }
	(rule__State__TransitionsAssignment_2)*
	{ after(grammarAccess.getStateAccess().getTransitionsAssignment_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Transition__Group__0
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Transition__Group__0__Impl
	rule__Transition__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__0__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getTransitionAccess().getWhenKeyword_0()); }
	'when'
	{ after(grammarAccess.getTransitionAccess().getWhenKeyword_0()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__1
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Transition__Group__1__Impl
	rule__Transition__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__1__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getTransitionAccess().getEventAssignment_1()); }
	(rule__Transition__EventAssignment_1)
	{ after(grammarAccess.getTransitionAccess().getEventAssignment_1()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__2
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Transition__Group__2__Impl
	rule__Transition__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__2__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getTransitionAccess().getChangeKeyword_2()); }
	'change'
	{ after(grammarAccess.getTransitionAccess().getChangeKeyword_2()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__3
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Transition__Group__3__Impl
	rule__Transition__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__3__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getTransitionAccess().getStatusKeyword_3()); }
	'status'
	{ after(grammarAccess.getTransitionAccess().getStatusKeyword_3()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__4
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Transition__Group__4__Impl
	rule__Transition__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__4__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getTransitionAccess().getToKeyword_4()); }
	'to'
	{ after(grammarAccess.getTransitionAccess().getToKeyword_4()); }
)
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__5
	@init {
		int stackSize = keepStackSize();
	}
:
	rule__Transition__Group__5__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__Group__5__Impl
	@init {
		int stackSize = keepStackSize();
	}
:
(
	{ before(grammarAccess.getTransitionAccess().getTargetAssignment_5()); }
	(rule__Transition__TargetAssignment_5)
	{ after(grammarAccess.getTransitionAccess().getTargetAssignment_5()); }
)
;
finally {
	restoreStackSize(stackSize);
}


rule__Piece__NameAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getPieceAccess().getNameIDTerminalRuleCall_1_0()); }
		RULE_ID
		{ after(grammarAccess.getPieceAccess().getNameIDTerminalRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Piece__StateMachineAssignment_6
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getPieceAccess().getStateMachineStateMachineParserRuleCall_6_0()); }
		ruleStateMachine
		{ after(grammarAccess.getPieceAccess().getStateMachineStateMachineParserRuleCall_6_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__FileAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getImageAccess().getFileSTRINGTerminalRuleCall_2_0()); }
		RULE_STRING
		{ after(grammarAccess.getImageAccess().getFileSTRINGTerminalRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Image__StateAssignment_4
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getImageAccess().getStateStateCrossReference_4_0()); }
		(
			{ before(grammarAccess.getImageAccess().getStateStateIDTerminalRuleCall_4_0_1()); }
			RULE_ID
			{ after(grammarAccess.getImageAccess().getStateStateIDTerminalRuleCall_4_0_1()); }
		)
		{ after(grammarAccess.getImageAccess().getStateStateCrossReference_4_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__ImagesAssignment_0_0
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getStateMachineAccess().getImagesImageParserRuleCall_0_0_0()); }
		ruleImage
		{ after(grammarAccess.getStateMachineAccess().getImagesImageParserRuleCall_0_0_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__StatesAssignment_0_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0_1_0()); }
		ruleState
		{ after(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__StateMachine__InitialStateAssignment_3
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getStateMachineAccess().getInitialStateStateCrossReference_3_0()); }
		(
			{ before(grammarAccess.getStateMachineAccess().getInitialStateStateIDTerminalRuleCall_3_0_1()); }
			RULE_ID
			{ after(grammarAccess.getStateMachineAccess().getInitialStateStateIDTerminalRuleCall_3_0_1()); }
		)
		{ after(grammarAccess.getStateMachineAccess().getInitialStateStateCrossReference_3_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__State__NameAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); }
		RULE_ID
		{ after(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__State__TransitionsAssignment_2
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); }
		ruleTransition
		{ after(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__EventAssignment_1
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0()); }
		RULE_ID
		{ after(grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

rule__Transition__TargetAssignment_5
	@init {
		int stackSize = keepStackSize();
	}
:
	(
		{ before(grammarAccess.getTransitionAccess().getTargetStateCrossReference_5_0()); }
		(
			{ before(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_5_0_1()); }
			RULE_ID
			{ after(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_5_0_1()); }
		)
		{ after(grammarAccess.getTransitionAccess().getTargetStateCrossReference_5_0()); }
	)
;
finally {
	restoreStackSize(stackSize);
}

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
