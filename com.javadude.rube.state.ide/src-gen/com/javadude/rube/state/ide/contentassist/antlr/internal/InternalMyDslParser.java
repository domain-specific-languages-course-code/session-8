package com.javadude.rube.state.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import com.javadude.rube.state.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'piece'", "'stuff'", "'that'", "'can'", "'happen'", "'show'", "'image'", "'for'", "'start'", "'here'", "'status'", "'when'", "'change'", "'to'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRulePiece"
    // InternalMyDsl.g:53:1: entryRulePiece : rulePiece EOF ;
    public final void entryRulePiece() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( rulePiece EOF )
            // InternalMyDsl.g:55:1: rulePiece EOF
            {
             before(grammarAccess.getPieceRule()); 
            pushFollow(FOLLOW_1);
            rulePiece();

            state._fsp--;

             after(grammarAccess.getPieceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePiece"


    // $ANTLR start "rulePiece"
    // InternalMyDsl.g:62:1: rulePiece : ( ( rule__Piece__Group__0 ) ) ;
    public final void rulePiece() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Piece__Group__0 ) ) )
            // InternalMyDsl.g:67:2: ( ( rule__Piece__Group__0 ) )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Piece__Group__0 ) )
            // InternalMyDsl.g:68:3: ( rule__Piece__Group__0 )
            {
             before(grammarAccess.getPieceAccess().getGroup()); 
            // InternalMyDsl.g:69:3: ( rule__Piece__Group__0 )
            // InternalMyDsl.g:69:4: rule__Piece__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Piece__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPieceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePiece"


    // $ANTLR start "entryRuleImage"
    // InternalMyDsl.g:78:1: entryRuleImage : ruleImage EOF ;
    public final void entryRuleImage() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleImage EOF )
            // InternalMyDsl.g:80:1: ruleImage EOF
            {
             before(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_1);
            ruleImage();

            state._fsp--;

             after(grammarAccess.getImageRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // InternalMyDsl.g:87:1: ruleImage : ( ( rule__Image__Group__0 ) ) ;
    public final void ruleImage() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Image__Group__0 ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Image__Group__0 ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Image__Group__0 ) )
            // InternalMyDsl.g:93:3: ( rule__Image__Group__0 )
            {
             before(grammarAccess.getImageAccess().getGroup()); 
            // InternalMyDsl.g:94:3: ( rule__Image__Group__0 )
            // InternalMyDsl.g:94:4: rule__Image__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Image__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleStateMachine"
    // InternalMyDsl.g:103:1: entryRuleStateMachine : ruleStateMachine EOF ;
    public final void entryRuleStateMachine() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleStateMachine EOF )
            // InternalMyDsl.g:105:1: ruleStateMachine EOF
            {
             before(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            ruleStateMachine();

            state._fsp--;

             after(grammarAccess.getStateMachineRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalMyDsl.g:112:1: ruleStateMachine : ( ( rule__StateMachine__Group__0 ) ) ;
    public final void ruleStateMachine() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__StateMachine__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__StateMachine__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__StateMachine__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__StateMachine__Group__0 )
            {
             before(grammarAccess.getStateMachineAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__StateMachine__Group__0 )
            // InternalMyDsl.g:119:4: rule__StateMachine__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleState"
    // InternalMyDsl.g:128:1: entryRuleState : ruleState EOF ;
    public final void entryRuleState() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleState EOF )
            // InternalMyDsl.g:130:1: ruleState EOF
            {
             before(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalMyDsl.g:137:1: ruleState : ( ( rule__State__Group__0 ) ) ;
    public final void ruleState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__State__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__State__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__State__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__State__Group__0 )
            {
             before(grammarAccess.getStateAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__State__Group__0 )
            // InternalMyDsl.g:144:4: rule__State__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalMyDsl.g:153:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleTransition EOF )
            // InternalMyDsl.g:155:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalMyDsl.g:162:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__Transition__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__Transition__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__Transition__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__Transition__Group__0 )
            // InternalMyDsl.g:169:4: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "rule__StateMachine__Alternatives_0"
    // InternalMyDsl.g:177:1: rule__StateMachine__Alternatives_0 : ( ( ( rule__StateMachine__ImagesAssignment_0_0 ) ) | ( ( rule__StateMachine__StatesAssignment_0_1 ) ) );
    public final void rule__StateMachine__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:181:1: ( ( ( rule__StateMachine__ImagesAssignment_0_0 ) ) | ( ( rule__StateMachine__StatesAssignment_0_1 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==16) ) {
                alt1=1;
            }
            else if ( (LA1_0==21) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalMyDsl.g:182:2: ( ( rule__StateMachine__ImagesAssignment_0_0 ) )
                    {
                    // InternalMyDsl.g:182:2: ( ( rule__StateMachine__ImagesAssignment_0_0 ) )
                    // InternalMyDsl.g:183:3: ( rule__StateMachine__ImagesAssignment_0_0 )
                    {
                     before(grammarAccess.getStateMachineAccess().getImagesAssignment_0_0()); 
                    // InternalMyDsl.g:184:3: ( rule__StateMachine__ImagesAssignment_0_0 )
                    // InternalMyDsl.g:184:4: rule__StateMachine__ImagesAssignment_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__StateMachine__ImagesAssignment_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getStateMachineAccess().getImagesAssignment_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:188:2: ( ( rule__StateMachine__StatesAssignment_0_1 ) )
                    {
                    // InternalMyDsl.g:188:2: ( ( rule__StateMachine__StatesAssignment_0_1 ) )
                    // InternalMyDsl.g:189:3: ( rule__StateMachine__StatesAssignment_0_1 )
                    {
                     before(grammarAccess.getStateMachineAccess().getStatesAssignment_0_1()); 
                    // InternalMyDsl.g:190:3: ( rule__StateMachine__StatesAssignment_0_1 )
                    // InternalMyDsl.g:190:4: rule__StateMachine__StatesAssignment_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__StateMachine__StatesAssignment_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getStateMachineAccess().getStatesAssignment_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Alternatives_0"


    // $ANTLR start "rule__Piece__Group__0"
    // InternalMyDsl.g:198:1: rule__Piece__Group__0 : rule__Piece__Group__0__Impl rule__Piece__Group__1 ;
    public final void rule__Piece__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:202:1: ( rule__Piece__Group__0__Impl rule__Piece__Group__1 )
            // InternalMyDsl.g:203:2: rule__Piece__Group__0__Impl rule__Piece__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Piece__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Piece__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__0"


    // $ANTLR start "rule__Piece__Group__0__Impl"
    // InternalMyDsl.g:210:1: rule__Piece__Group__0__Impl : ( 'piece' ) ;
    public final void rule__Piece__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:214:1: ( ( 'piece' ) )
            // InternalMyDsl.g:215:1: ( 'piece' )
            {
            // InternalMyDsl.g:215:1: ( 'piece' )
            // InternalMyDsl.g:216:2: 'piece'
            {
             before(grammarAccess.getPieceAccess().getPieceKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getPieceAccess().getPieceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__0__Impl"


    // $ANTLR start "rule__Piece__Group__1"
    // InternalMyDsl.g:225:1: rule__Piece__Group__1 : rule__Piece__Group__1__Impl rule__Piece__Group__2 ;
    public final void rule__Piece__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:229:1: ( rule__Piece__Group__1__Impl rule__Piece__Group__2 )
            // InternalMyDsl.g:230:2: rule__Piece__Group__1__Impl rule__Piece__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Piece__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Piece__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__1"


    // $ANTLR start "rule__Piece__Group__1__Impl"
    // InternalMyDsl.g:237:1: rule__Piece__Group__1__Impl : ( ( rule__Piece__NameAssignment_1 ) ) ;
    public final void rule__Piece__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:1: ( ( ( rule__Piece__NameAssignment_1 ) ) )
            // InternalMyDsl.g:242:1: ( ( rule__Piece__NameAssignment_1 ) )
            {
            // InternalMyDsl.g:242:1: ( ( rule__Piece__NameAssignment_1 ) )
            // InternalMyDsl.g:243:2: ( rule__Piece__NameAssignment_1 )
            {
             before(grammarAccess.getPieceAccess().getNameAssignment_1()); 
            // InternalMyDsl.g:244:2: ( rule__Piece__NameAssignment_1 )
            // InternalMyDsl.g:244:3: rule__Piece__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Piece__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPieceAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__1__Impl"


    // $ANTLR start "rule__Piece__Group__2"
    // InternalMyDsl.g:252:1: rule__Piece__Group__2 : rule__Piece__Group__2__Impl rule__Piece__Group__3 ;
    public final void rule__Piece__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:256:1: ( rule__Piece__Group__2__Impl rule__Piece__Group__3 )
            // InternalMyDsl.g:257:2: rule__Piece__Group__2__Impl rule__Piece__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Piece__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Piece__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__2"


    // $ANTLR start "rule__Piece__Group__2__Impl"
    // InternalMyDsl.g:264:1: rule__Piece__Group__2__Impl : ( 'stuff' ) ;
    public final void rule__Piece__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:268:1: ( ( 'stuff' ) )
            // InternalMyDsl.g:269:1: ( 'stuff' )
            {
            // InternalMyDsl.g:269:1: ( 'stuff' )
            // InternalMyDsl.g:270:2: 'stuff'
            {
             before(grammarAccess.getPieceAccess().getStuffKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getPieceAccess().getStuffKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__2__Impl"


    // $ANTLR start "rule__Piece__Group__3"
    // InternalMyDsl.g:279:1: rule__Piece__Group__3 : rule__Piece__Group__3__Impl rule__Piece__Group__4 ;
    public final void rule__Piece__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:283:1: ( rule__Piece__Group__3__Impl rule__Piece__Group__4 )
            // InternalMyDsl.g:284:2: rule__Piece__Group__3__Impl rule__Piece__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Piece__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Piece__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__3"


    // $ANTLR start "rule__Piece__Group__3__Impl"
    // InternalMyDsl.g:291:1: rule__Piece__Group__3__Impl : ( 'that' ) ;
    public final void rule__Piece__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:295:1: ( ( 'that' ) )
            // InternalMyDsl.g:296:1: ( 'that' )
            {
            // InternalMyDsl.g:296:1: ( 'that' )
            // InternalMyDsl.g:297:2: 'that'
            {
             before(grammarAccess.getPieceAccess().getThatKeyword_3()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getPieceAccess().getThatKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__3__Impl"


    // $ANTLR start "rule__Piece__Group__4"
    // InternalMyDsl.g:306:1: rule__Piece__Group__4 : rule__Piece__Group__4__Impl rule__Piece__Group__5 ;
    public final void rule__Piece__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:310:1: ( rule__Piece__Group__4__Impl rule__Piece__Group__5 )
            // InternalMyDsl.g:311:2: rule__Piece__Group__4__Impl rule__Piece__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Piece__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Piece__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__4"


    // $ANTLR start "rule__Piece__Group__4__Impl"
    // InternalMyDsl.g:318:1: rule__Piece__Group__4__Impl : ( 'can' ) ;
    public final void rule__Piece__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:322:1: ( ( 'can' ) )
            // InternalMyDsl.g:323:1: ( 'can' )
            {
            // InternalMyDsl.g:323:1: ( 'can' )
            // InternalMyDsl.g:324:2: 'can'
            {
             before(grammarAccess.getPieceAccess().getCanKeyword_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getPieceAccess().getCanKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__4__Impl"


    // $ANTLR start "rule__Piece__Group__5"
    // InternalMyDsl.g:333:1: rule__Piece__Group__5 : rule__Piece__Group__5__Impl rule__Piece__Group__6 ;
    public final void rule__Piece__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:337:1: ( rule__Piece__Group__5__Impl rule__Piece__Group__6 )
            // InternalMyDsl.g:338:2: rule__Piece__Group__5__Impl rule__Piece__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__Piece__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Piece__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__5"


    // $ANTLR start "rule__Piece__Group__5__Impl"
    // InternalMyDsl.g:345:1: rule__Piece__Group__5__Impl : ( 'happen' ) ;
    public final void rule__Piece__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:349:1: ( ( 'happen' ) )
            // InternalMyDsl.g:350:1: ( 'happen' )
            {
            // InternalMyDsl.g:350:1: ( 'happen' )
            // InternalMyDsl.g:351:2: 'happen'
            {
             before(grammarAccess.getPieceAccess().getHappenKeyword_5()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPieceAccess().getHappenKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__5__Impl"


    // $ANTLR start "rule__Piece__Group__6"
    // InternalMyDsl.g:360:1: rule__Piece__Group__6 : rule__Piece__Group__6__Impl ;
    public final void rule__Piece__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:364:1: ( rule__Piece__Group__6__Impl )
            // InternalMyDsl.g:365:2: rule__Piece__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Piece__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__6"


    // $ANTLR start "rule__Piece__Group__6__Impl"
    // InternalMyDsl.g:371:1: rule__Piece__Group__6__Impl : ( ( rule__Piece__StateMachineAssignment_6 ) ) ;
    public final void rule__Piece__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:375:1: ( ( ( rule__Piece__StateMachineAssignment_6 ) ) )
            // InternalMyDsl.g:376:1: ( ( rule__Piece__StateMachineAssignment_6 ) )
            {
            // InternalMyDsl.g:376:1: ( ( rule__Piece__StateMachineAssignment_6 ) )
            // InternalMyDsl.g:377:2: ( rule__Piece__StateMachineAssignment_6 )
            {
             before(grammarAccess.getPieceAccess().getStateMachineAssignment_6()); 
            // InternalMyDsl.g:378:2: ( rule__Piece__StateMachineAssignment_6 )
            // InternalMyDsl.g:378:3: rule__Piece__StateMachineAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Piece__StateMachineAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getPieceAccess().getStateMachineAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__Group__6__Impl"


    // $ANTLR start "rule__Image__Group__0"
    // InternalMyDsl.g:387:1: rule__Image__Group__0 : rule__Image__Group__0__Impl rule__Image__Group__1 ;
    public final void rule__Image__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:391:1: ( rule__Image__Group__0__Impl rule__Image__Group__1 )
            // InternalMyDsl.g:392:2: rule__Image__Group__0__Impl rule__Image__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Image__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Image__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0"


    // $ANTLR start "rule__Image__Group__0__Impl"
    // InternalMyDsl.g:399:1: rule__Image__Group__0__Impl : ( 'show' ) ;
    public final void rule__Image__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:403:1: ( ( 'show' ) )
            // InternalMyDsl.g:404:1: ( 'show' )
            {
            // InternalMyDsl.g:404:1: ( 'show' )
            // InternalMyDsl.g:405:2: 'show'
            {
             before(grammarAccess.getImageAccess().getShowKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getImageAccess().getShowKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0__Impl"


    // $ANTLR start "rule__Image__Group__1"
    // InternalMyDsl.g:414:1: rule__Image__Group__1 : rule__Image__Group__1__Impl rule__Image__Group__2 ;
    public final void rule__Image__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:418:1: ( rule__Image__Group__1__Impl rule__Image__Group__2 )
            // InternalMyDsl.g:419:2: rule__Image__Group__1__Impl rule__Image__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__Image__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Image__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1"


    // $ANTLR start "rule__Image__Group__1__Impl"
    // InternalMyDsl.g:426:1: rule__Image__Group__1__Impl : ( 'image' ) ;
    public final void rule__Image__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:430:1: ( ( 'image' ) )
            // InternalMyDsl.g:431:1: ( 'image' )
            {
            // InternalMyDsl.g:431:1: ( 'image' )
            // InternalMyDsl.g:432:2: 'image'
            {
             before(grammarAccess.getImageAccess().getImageKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getImageAccess().getImageKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1__Impl"


    // $ANTLR start "rule__Image__Group__2"
    // InternalMyDsl.g:441:1: rule__Image__Group__2 : rule__Image__Group__2__Impl rule__Image__Group__3 ;
    public final void rule__Image__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:445:1: ( rule__Image__Group__2__Impl rule__Image__Group__3 )
            // InternalMyDsl.g:446:2: rule__Image__Group__2__Impl rule__Image__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__Image__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Image__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__2"


    // $ANTLR start "rule__Image__Group__2__Impl"
    // InternalMyDsl.g:453:1: rule__Image__Group__2__Impl : ( ( rule__Image__FileAssignment_2 ) ) ;
    public final void rule__Image__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:457:1: ( ( ( rule__Image__FileAssignment_2 ) ) )
            // InternalMyDsl.g:458:1: ( ( rule__Image__FileAssignment_2 ) )
            {
            // InternalMyDsl.g:458:1: ( ( rule__Image__FileAssignment_2 ) )
            // InternalMyDsl.g:459:2: ( rule__Image__FileAssignment_2 )
            {
             before(grammarAccess.getImageAccess().getFileAssignment_2()); 
            // InternalMyDsl.g:460:2: ( rule__Image__FileAssignment_2 )
            // InternalMyDsl.g:460:3: rule__Image__FileAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Image__FileAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getFileAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__2__Impl"


    // $ANTLR start "rule__Image__Group__3"
    // InternalMyDsl.g:468:1: rule__Image__Group__3 : rule__Image__Group__3__Impl rule__Image__Group__4 ;
    public final void rule__Image__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:472:1: ( rule__Image__Group__3__Impl rule__Image__Group__4 )
            // InternalMyDsl.g:473:2: rule__Image__Group__3__Impl rule__Image__Group__4
            {
            pushFollow(FOLLOW_3);
            rule__Image__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Image__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__3"


    // $ANTLR start "rule__Image__Group__3__Impl"
    // InternalMyDsl.g:480:1: rule__Image__Group__3__Impl : ( 'for' ) ;
    public final void rule__Image__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:484:1: ( ( 'for' ) )
            // InternalMyDsl.g:485:1: ( 'for' )
            {
            // InternalMyDsl.g:485:1: ( 'for' )
            // InternalMyDsl.g:486:2: 'for'
            {
             before(grammarAccess.getImageAccess().getForKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getImageAccess().getForKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__3__Impl"


    // $ANTLR start "rule__Image__Group__4"
    // InternalMyDsl.g:495:1: rule__Image__Group__4 : rule__Image__Group__4__Impl ;
    public final void rule__Image__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:499:1: ( rule__Image__Group__4__Impl )
            // InternalMyDsl.g:500:2: rule__Image__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Image__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__4"


    // $ANTLR start "rule__Image__Group__4__Impl"
    // InternalMyDsl.g:506:1: rule__Image__Group__4__Impl : ( ( rule__Image__StateAssignment_4 ) ) ;
    public final void rule__Image__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:510:1: ( ( ( rule__Image__StateAssignment_4 ) ) )
            // InternalMyDsl.g:511:1: ( ( rule__Image__StateAssignment_4 ) )
            {
            // InternalMyDsl.g:511:1: ( ( rule__Image__StateAssignment_4 ) )
            // InternalMyDsl.g:512:2: ( rule__Image__StateAssignment_4 )
            {
             before(grammarAccess.getImageAccess().getStateAssignment_4()); 
            // InternalMyDsl.g:513:2: ( rule__Image__StateAssignment_4 )
            // InternalMyDsl.g:513:3: rule__Image__StateAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Image__StateAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getStateAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__4__Impl"


    // $ANTLR start "rule__StateMachine__Group__0"
    // InternalMyDsl.g:522:1: rule__StateMachine__Group__0 : rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 ;
    public final void rule__StateMachine__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:526:1: ( rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1 )
            // InternalMyDsl.g:527:2: rule__StateMachine__Group__0__Impl rule__StateMachine__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__StateMachine__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0"


    // $ANTLR start "rule__StateMachine__Group__0__Impl"
    // InternalMyDsl.g:534:1: rule__StateMachine__Group__0__Impl : ( ( rule__StateMachine__Alternatives_0 )* ) ;
    public final void rule__StateMachine__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:538:1: ( ( ( rule__StateMachine__Alternatives_0 )* ) )
            // InternalMyDsl.g:539:1: ( ( rule__StateMachine__Alternatives_0 )* )
            {
            // InternalMyDsl.g:539:1: ( ( rule__StateMachine__Alternatives_0 )* )
            // InternalMyDsl.g:540:2: ( rule__StateMachine__Alternatives_0 )*
            {
             before(grammarAccess.getStateMachineAccess().getAlternatives_0()); 
            // InternalMyDsl.g:541:2: ( rule__StateMachine__Alternatives_0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==16||LA2_0==21) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMyDsl.g:541:3: rule__StateMachine__Alternatives_0
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__StateMachine__Alternatives_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__0__Impl"


    // $ANTLR start "rule__StateMachine__Group__1"
    // InternalMyDsl.g:549:1: rule__StateMachine__Group__1 : rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2 ;
    public final void rule__StateMachine__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:553:1: ( rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2 )
            // InternalMyDsl.g:554:2: rule__StateMachine__Group__1__Impl rule__StateMachine__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__StateMachine__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1"


    // $ANTLR start "rule__StateMachine__Group__1__Impl"
    // InternalMyDsl.g:561:1: rule__StateMachine__Group__1__Impl : ( 'start' ) ;
    public final void rule__StateMachine__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:565:1: ( ( 'start' ) )
            // InternalMyDsl.g:566:1: ( 'start' )
            {
            // InternalMyDsl.g:566:1: ( 'start' )
            // InternalMyDsl.g:567:2: 'start'
            {
             before(grammarAccess.getStateMachineAccess().getStartKeyword_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getStartKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__1__Impl"


    // $ANTLR start "rule__StateMachine__Group__2"
    // InternalMyDsl.g:576:1: rule__StateMachine__Group__2 : rule__StateMachine__Group__2__Impl rule__StateMachine__Group__3 ;
    public final void rule__StateMachine__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:580:1: ( rule__StateMachine__Group__2__Impl rule__StateMachine__Group__3 )
            // InternalMyDsl.g:581:2: rule__StateMachine__Group__2__Impl rule__StateMachine__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__StateMachine__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__2"


    // $ANTLR start "rule__StateMachine__Group__2__Impl"
    // InternalMyDsl.g:588:1: rule__StateMachine__Group__2__Impl : ( 'here' ) ;
    public final void rule__StateMachine__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:592:1: ( ( 'here' ) )
            // InternalMyDsl.g:593:1: ( 'here' )
            {
            // InternalMyDsl.g:593:1: ( 'here' )
            // InternalMyDsl.g:594:2: 'here'
            {
             before(grammarAccess.getStateMachineAccess().getHereKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getHereKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__2__Impl"


    // $ANTLR start "rule__StateMachine__Group__3"
    // InternalMyDsl.g:603:1: rule__StateMachine__Group__3 : rule__StateMachine__Group__3__Impl ;
    public final void rule__StateMachine__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:607:1: ( rule__StateMachine__Group__3__Impl )
            // InternalMyDsl.g:608:2: rule__StateMachine__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__3"


    // $ANTLR start "rule__StateMachine__Group__3__Impl"
    // InternalMyDsl.g:614:1: rule__StateMachine__Group__3__Impl : ( ( rule__StateMachine__InitialStateAssignment_3 ) ) ;
    public final void rule__StateMachine__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:618:1: ( ( ( rule__StateMachine__InitialStateAssignment_3 ) ) )
            // InternalMyDsl.g:619:1: ( ( rule__StateMachine__InitialStateAssignment_3 ) )
            {
            // InternalMyDsl.g:619:1: ( ( rule__StateMachine__InitialStateAssignment_3 ) )
            // InternalMyDsl.g:620:2: ( rule__StateMachine__InitialStateAssignment_3 )
            {
             before(grammarAccess.getStateMachineAccess().getInitialStateAssignment_3()); 
            // InternalMyDsl.g:621:2: ( rule__StateMachine__InitialStateAssignment_3 )
            // InternalMyDsl.g:621:3: rule__StateMachine__InitialStateAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__StateMachine__InitialStateAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getStateMachineAccess().getInitialStateAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__Group__3__Impl"


    // $ANTLR start "rule__State__Group__0"
    // InternalMyDsl.g:630:1: rule__State__Group__0 : rule__State__Group__0__Impl rule__State__Group__1 ;
    public final void rule__State__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:634:1: ( rule__State__Group__0__Impl rule__State__Group__1 )
            // InternalMyDsl.g:635:2: rule__State__Group__0__Impl rule__State__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__State__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0"


    // $ANTLR start "rule__State__Group__0__Impl"
    // InternalMyDsl.g:642:1: rule__State__Group__0__Impl : ( 'status' ) ;
    public final void rule__State__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:646:1: ( ( 'status' ) )
            // InternalMyDsl.g:647:1: ( 'status' )
            {
            // InternalMyDsl.g:647:1: ( 'status' )
            // InternalMyDsl.g:648:2: 'status'
            {
             before(grammarAccess.getStateAccess().getStatusKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getStatusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0__Impl"


    // $ANTLR start "rule__State__Group__1"
    // InternalMyDsl.g:657:1: rule__State__Group__1 : rule__State__Group__1__Impl rule__State__Group__2 ;
    public final void rule__State__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:661:1: ( rule__State__Group__1__Impl rule__State__Group__2 )
            // InternalMyDsl.g:662:2: rule__State__Group__1__Impl rule__State__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__State__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1"


    // $ANTLR start "rule__State__Group__1__Impl"
    // InternalMyDsl.g:669:1: rule__State__Group__1__Impl : ( ( rule__State__NameAssignment_1 ) ) ;
    public final void rule__State__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:673:1: ( ( ( rule__State__NameAssignment_1 ) ) )
            // InternalMyDsl.g:674:1: ( ( rule__State__NameAssignment_1 ) )
            {
            // InternalMyDsl.g:674:1: ( ( rule__State__NameAssignment_1 ) )
            // InternalMyDsl.g:675:2: ( rule__State__NameAssignment_1 )
            {
             before(grammarAccess.getStateAccess().getNameAssignment_1()); 
            // InternalMyDsl.g:676:2: ( rule__State__NameAssignment_1 )
            // InternalMyDsl.g:676:3: rule__State__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__State__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1__Impl"


    // $ANTLR start "rule__State__Group__2"
    // InternalMyDsl.g:684:1: rule__State__Group__2 : rule__State__Group__2__Impl ;
    public final void rule__State__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:688:1: ( rule__State__Group__2__Impl )
            // InternalMyDsl.g:689:2: rule__State__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2"


    // $ANTLR start "rule__State__Group__2__Impl"
    // InternalMyDsl.g:695:1: rule__State__Group__2__Impl : ( ( rule__State__TransitionsAssignment_2 )* ) ;
    public final void rule__State__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:699:1: ( ( ( rule__State__TransitionsAssignment_2 )* ) )
            // InternalMyDsl.g:700:1: ( ( rule__State__TransitionsAssignment_2 )* )
            {
            // InternalMyDsl.g:700:1: ( ( rule__State__TransitionsAssignment_2 )* )
            // InternalMyDsl.g:701:2: ( rule__State__TransitionsAssignment_2 )*
            {
             before(grammarAccess.getStateAccess().getTransitionsAssignment_2()); 
            // InternalMyDsl.g:702:2: ( rule__State__TransitionsAssignment_2 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==22) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:702:3: rule__State__TransitionsAssignment_2
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__State__TransitionsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getStateAccess().getTransitionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // InternalMyDsl.g:711:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:715:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // InternalMyDsl.g:716:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // InternalMyDsl.g:723:1: rule__Transition__Group__0__Impl : ( 'when' ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:727:1: ( ( 'when' ) )
            // InternalMyDsl.g:728:1: ( 'when' )
            {
            // InternalMyDsl.g:728:1: ( 'when' )
            // InternalMyDsl.g:729:2: 'when'
            {
             before(grammarAccess.getTransitionAccess().getWhenKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getWhenKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // InternalMyDsl.g:738:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:742:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // InternalMyDsl.g:743:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // InternalMyDsl.g:750:1: rule__Transition__Group__1__Impl : ( ( rule__Transition__EventAssignment_1 ) ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:754:1: ( ( ( rule__Transition__EventAssignment_1 ) ) )
            // InternalMyDsl.g:755:1: ( ( rule__Transition__EventAssignment_1 ) )
            {
            // InternalMyDsl.g:755:1: ( ( rule__Transition__EventAssignment_1 ) )
            // InternalMyDsl.g:756:2: ( rule__Transition__EventAssignment_1 )
            {
             before(grammarAccess.getTransitionAccess().getEventAssignment_1()); 
            // InternalMyDsl.g:757:2: ( rule__Transition__EventAssignment_1 )
            // InternalMyDsl.g:757:3: rule__Transition__EventAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Transition__EventAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getEventAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // InternalMyDsl.g:765:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:769:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // InternalMyDsl.g:770:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__Transition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // InternalMyDsl.g:777:1: rule__Transition__Group__2__Impl : ( 'change' ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:781:1: ( ( 'change' ) )
            // InternalMyDsl.g:782:1: ( 'change' )
            {
            // InternalMyDsl.g:782:1: ( 'change' )
            // InternalMyDsl.g:783:2: 'change'
            {
             before(grammarAccess.getTransitionAccess().getChangeKeyword_2()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getChangeKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // InternalMyDsl.g:792:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl rule__Transition__Group__4 ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:796:1: ( rule__Transition__Group__3__Impl rule__Transition__Group__4 )
            // InternalMyDsl.g:797:2: rule__Transition__Group__3__Impl rule__Transition__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__Transition__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // InternalMyDsl.g:804:1: rule__Transition__Group__3__Impl : ( 'status' ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:808:1: ( ( 'status' ) )
            // InternalMyDsl.g:809:1: ( 'status' )
            {
            // InternalMyDsl.g:809:1: ( 'status' )
            // InternalMyDsl.g:810:2: 'status'
            {
             before(grammarAccess.getTransitionAccess().getStatusKeyword_3()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getStatusKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group__4"
    // InternalMyDsl.g:819:1: rule__Transition__Group__4 : rule__Transition__Group__4__Impl rule__Transition__Group__5 ;
    public final void rule__Transition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:823:1: ( rule__Transition__Group__4__Impl rule__Transition__Group__5 )
            // InternalMyDsl.g:824:2: rule__Transition__Group__4__Impl rule__Transition__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__Transition__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4"


    // $ANTLR start "rule__Transition__Group__4__Impl"
    // InternalMyDsl.g:831:1: rule__Transition__Group__4__Impl : ( 'to' ) ;
    public final void rule__Transition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:835:1: ( ( 'to' ) )
            // InternalMyDsl.g:836:1: ( 'to' )
            {
            // InternalMyDsl.g:836:1: ( 'to' )
            // InternalMyDsl.g:837:2: 'to'
            {
             before(grammarAccess.getTransitionAccess().getToKeyword_4()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getToKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4__Impl"


    // $ANTLR start "rule__Transition__Group__5"
    // InternalMyDsl.g:846:1: rule__Transition__Group__5 : rule__Transition__Group__5__Impl ;
    public final void rule__Transition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:850:1: ( rule__Transition__Group__5__Impl )
            // InternalMyDsl.g:851:2: rule__Transition__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5"


    // $ANTLR start "rule__Transition__Group__5__Impl"
    // InternalMyDsl.g:857:1: rule__Transition__Group__5__Impl : ( ( rule__Transition__TargetAssignment_5 ) ) ;
    public final void rule__Transition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:861:1: ( ( ( rule__Transition__TargetAssignment_5 ) ) )
            // InternalMyDsl.g:862:1: ( ( rule__Transition__TargetAssignment_5 ) )
            {
            // InternalMyDsl.g:862:1: ( ( rule__Transition__TargetAssignment_5 ) )
            // InternalMyDsl.g:863:2: ( rule__Transition__TargetAssignment_5 )
            {
             before(grammarAccess.getTransitionAccess().getTargetAssignment_5()); 
            // InternalMyDsl.g:864:2: ( rule__Transition__TargetAssignment_5 )
            // InternalMyDsl.g:864:3: rule__Transition__TargetAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Transition__TargetAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getTargetAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__5__Impl"


    // $ANTLR start "rule__Piece__NameAssignment_1"
    // InternalMyDsl.g:873:1: rule__Piece__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Piece__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:877:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:878:2: ( RULE_ID )
            {
            // InternalMyDsl.g:878:2: ( RULE_ID )
            // InternalMyDsl.g:879:3: RULE_ID
            {
             before(grammarAccess.getPieceAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPieceAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__NameAssignment_1"


    // $ANTLR start "rule__Piece__StateMachineAssignment_6"
    // InternalMyDsl.g:888:1: rule__Piece__StateMachineAssignment_6 : ( ruleStateMachine ) ;
    public final void rule__Piece__StateMachineAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:892:1: ( ( ruleStateMachine ) )
            // InternalMyDsl.g:893:2: ( ruleStateMachine )
            {
            // InternalMyDsl.g:893:2: ( ruleStateMachine )
            // InternalMyDsl.g:894:3: ruleStateMachine
            {
             before(grammarAccess.getPieceAccess().getStateMachineStateMachineParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleStateMachine();

            state._fsp--;

             after(grammarAccess.getPieceAccess().getStateMachineStateMachineParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Piece__StateMachineAssignment_6"


    // $ANTLR start "rule__Image__FileAssignment_2"
    // InternalMyDsl.g:903:1: rule__Image__FileAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Image__FileAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:907:1: ( ( RULE_STRING ) )
            // InternalMyDsl.g:908:2: ( RULE_STRING )
            {
            // InternalMyDsl.g:908:2: ( RULE_STRING )
            // InternalMyDsl.g:909:3: RULE_STRING
            {
             before(grammarAccess.getImageAccess().getFileSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImageAccess().getFileSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__FileAssignment_2"


    // $ANTLR start "rule__Image__StateAssignment_4"
    // InternalMyDsl.g:918:1: rule__Image__StateAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__Image__StateAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:922:1: ( ( ( RULE_ID ) ) )
            // InternalMyDsl.g:923:2: ( ( RULE_ID ) )
            {
            // InternalMyDsl.g:923:2: ( ( RULE_ID ) )
            // InternalMyDsl.g:924:3: ( RULE_ID )
            {
             before(grammarAccess.getImageAccess().getStateStateCrossReference_4_0()); 
            // InternalMyDsl.g:925:3: ( RULE_ID )
            // InternalMyDsl.g:926:4: RULE_ID
            {
             before(grammarAccess.getImageAccess().getStateStateIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getImageAccess().getStateStateIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getImageAccess().getStateStateCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__StateAssignment_4"


    // $ANTLR start "rule__StateMachine__ImagesAssignment_0_0"
    // InternalMyDsl.g:937:1: rule__StateMachine__ImagesAssignment_0_0 : ( ruleImage ) ;
    public final void rule__StateMachine__ImagesAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:941:1: ( ( ruleImage ) )
            // InternalMyDsl.g:942:2: ( ruleImage )
            {
            // InternalMyDsl.g:942:2: ( ruleImage )
            // InternalMyDsl.g:943:3: ruleImage
            {
             before(grammarAccess.getStateMachineAccess().getImagesImageParserRuleCall_0_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImage();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getImagesImageParserRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__ImagesAssignment_0_0"


    // $ANTLR start "rule__StateMachine__StatesAssignment_0_1"
    // InternalMyDsl.g:952:1: rule__StateMachine__StatesAssignment_0_1 : ( ruleState ) ;
    public final void rule__StateMachine__StatesAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:956:1: ( ( ruleState ) )
            // InternalMyDsl.g:957:2: ( ruleState )
            {
            // InternalMyDsl.g:957:2: ( ruleState )
            // InternalMyDsl.g:958:3: ruleState
            {
             before(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__StatesAssignment_0_1"


    // $ANTLR start "rule__StateMachine__InitialStateAssignment_3"
    // InternalMyDsl.g:967:1: rule__StateMachine__InitialStateAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__StateMachine__InitialStateAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:971:1: ( ( ( RULE_ID ) ) )
            // InternalMyDsl.g:972:2: ( ( RULE_ID ) )
            {
            // InternalMyDsl.g:972:2: ( ( RULE_ID ) )
            // InternalMyDsl.g:973:3: ( RULE_ID )
            {
             before(grammarAccess.getStateMachineAccess().getInitialStateStateCrossReference_3_0()); 
            // InternalMyDsl.g:974:3: ( RULE_ID )
            // InternalMyDsl.g:975:4: RULE_ID
            {
             before(grammarAccess.getStateMachineAccess().getInitialStateStateIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStateMachineAccess().getInitialStateStateIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getStateMachineAccess().getInitialStateStateCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__InitialStateAssignment_3"


    // $ANTLR start "rule__State__NameAssignment_1"
    // InternalMyDsl.g:986:1: rule__State__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__State__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:990:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:991:2: ( RULE_ID )
            {
            // InternalMyDsl.g:991:2: ( RULE_ID )
            // InternalMyDsl.g:992:3: RULE_ID
            {
             before(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__NameAssignment_1"


    // $ANTLR start "rule__State__TransitionsAssignment_2"
    // InternalMyDsl.g:1001:1: rule__State__TransitionsAssignment_2 : ( ruleTransition ) ;
    public final void rule__State__TransitionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1005:1: ( ( ruleTransition ) )
            // InternalMyDsl.g:1006:2: ( ruleTransition )
            {
            // InternalMyDsl.g:1006:2: ( ruleTransition )
            // InternalMyDsl.g:1007:3: ruleTransition
            {
             before(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__TransitionsAssignment_2"


    // $ANTLR start "rule__Transition__EventAssignment_1"
    // InternalMyDsl.g:1016:1: rule__Transition__EventAssignment_1 : ( RULE_ID ) ;
    public final void rule__Transition__EventAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1020:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1021:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1021:2: ( RULE_ID )
            // InternalMyDsl.g:1022:3: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__EventAssignment_1"


    // $ANTLR start "rule__Transition__TargetAssignment_5"
    // InternalMyDsl.g:1031:1: rule__Transition__TargetAssignment_5 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__TargetAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1035:1: ( ( ( RULE_ID ) ) )
            // InternalMyDsl.g:1036:2: ( ( RULE_ID ) )
            {
            // InternalMyDsl.g:1036:2: ( ( RULE_ID ) )
            // InternalMyDsl.g:1037:3: ( RULE_ID )
            {
             before(grammarAccess.getTransitionAccess().getTargetStateCrossReference_5_0()); 
            // InternalMyDsl.g:1038:3: ( RULE_ID )
            // InternalMyDsl.g:1039:4: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_5_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_5_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getTargetStateCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__TargetAssignment_5"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000290000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000210002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001000000L});

}