package com.javadude.state1.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import com.javadude.state1.services.State1DslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalState1DslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'state'", "'on'", "'goto'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalState1DslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalState1DslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalState1DslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalState1Dsl.g"; }


    	private State1DslGrammarAccess grammarAccess;

    	public void setGrammarAccess(State1DslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleStateMachine"
    // InternalState1Dsl.g:53:1: entryRuleStateMachine : ruleStateMachine EOF ;
    public final void entryRuleStateMachine() throws RecognitionException {
        try {
            // InternalState1Dsl.g:54:1: ( ruleStateMachine EOF )
            // InternalState1Dsl.g:55:1: ruleStateMachine EOF
            {
             before(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            ruleStateMachine();

            state._fsp--;

             after(grammarAccess.getStateMachineRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalState1Dsl.g:62:1: ruleStateMachine : ( ( rule__StateMachine__StatesAssignment )* ) ;
    public final void ruleStateMachine() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:66:2: ( ( ( rule__StateMachine__StatesAssignment )* ) )
            // InternalState1Dsl.g:67:2: ( ( rule__StateMachine__StatesAssignment )* )
            {
            // InternalState1Dsl.g:67:2: ( ( rule__StateMachine__StatesAssignment )* )
            // InternalState1Dsl.g:68:3: ( rule__StateMachine__StatesAssignment )*
            {
             before(grammarAccess.getStateMachineAccess().getStatesAssignment()); 
            // InternalState1Dsl.g:69:3: ( rule__StateMachine__StatesAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalState1Dsl.g:69:4: rule__StateMachine__StatesAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__StateMachine__StatesAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getStateMachineAccess().getStatesAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleState"
    // InternalState1Dsl.g:78:1: entryRuleState : ruleState EOF ;
    public final void entryRuleState() throws RecognitionException {
        try {
            // InternalState1Dsl.g:79:1: ( ruleState EOF )
            // InternalState1Dsl.g:80:1: ruleState EOF
            {
             before(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalState1Dsl.g:87:1: ruleState : ( ( rule__State__Group__0 ) ) ;
    public final void ruleState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:91:2: ( ( ( rule__State__Group__0 ) ) )
            // InternalState1Dsl.g:92:2: ( ( rule__State__Group__0 ) )
            {
            // InternalState1Dsl.g:92:2: ( ( rule__State__Group__0 ) )
            // InternalState1Dsl.g:93:3: ( rule__State__Group__0 )
            {
             before(grammarAccess.getStateAccess().getGroup()); 
            // InternalState1Dsl.g:94:3: ( rule__State__Group__0 )
            // InternalState1Dsl.g:94:4: rule__State__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalState1Dsl.g:103:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // InternalState1Dsl.g:104:1: ( ruleTransition EOF )
            // InternalState1Dsl.g:105:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalState1Dsl.g:112:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:116:2: ( ( ( rule__Transition__Group__0 ) ) )
            // InternalState1Dsl.g:117:2: ( ( rule__Transition__Group__0 ) )
            {
            // InternalState1Dsl.g:117:2: ( ( rule__Transition__Group__0 ) )
            // InternalState1Dsl.g:118:3: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // InternalState1Dsl.g:119:3: ( rule__Transition__Group__0 )
            // InternalState1Dsl.g:119:4: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "rule__State__Group__0"
    // InternalState1Dsl.g:127:1: rule__State__Group__0 : rule__State__Group__0__Impl rule__State__Group__1 ;
    public final void rule__State__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:131:1: ( rule__State__Group__0__Impl rule__State__Group__1 )
            // InternalState1Dsl.g:132:2: rule__State__Group__0__Impl rule__State__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__State__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0"


    // $ANTLR start "rule__State__Group__0__Impl"
    // InternalState1Dsl.g:139:1: rule__State__Group__0__Impl : ( 'state' ) ;
    public final void rule__State__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:143:1: ( ( 'state' ) )
            // InternalState1Dsl.g:144:1: ( 'state' )
            {
            // InternalState1Dsl.g:144:1: ( 'state' )
            // InternalState1Dsl.g:145:2: 'state'
            {
             before(grammarAccess.getStateAccess().getStateKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getStateKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0__Impl"


    // $ANTLR start "rule__State__Group__1"
    // InternalState1Dsl.g:154:1: rule__State__Group__1 : rule__State__Group__1__Impl rule__State__Group__2 ;
    public final void rule__State__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:158:1: ( rule__State__Group__1__Impl rule__State__Group__2 )
            // InternalState1Dsl.g:159:2: rule__State__Group__1__Impl rule__State__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__State__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1"


    // $ANTLR start "rule__State__Group__1__Impl"
    // InternalState1Dsl.g:166:1: rule__State__Group__1__Impl : ( ( rule__State__NameAssignment_1 ) ) ;
    public final void rule__State__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:170:1: ( ( ( rule__State__NameAssignment_1 ) ) )
            // InternalState1Dsl.g:171:1: ( ( rule__State__NameAssignment_1 ) )
            {
            // InternalState1Dsl.g:171:1: ( ( rule__State__NameAssignment_1 ) )
            // InternalState1Dsl.g:172:2: ( rule__State__NameAssignment_1 )
            {
             before(grammarAccess.getStateAccess().getNameAssignment_1()); 
            // InternalState1Dsl.g:173:2: ( rule__State__NameAssignment_1 )
            // InternalState1Dsl.g:173:3: rule__State__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__State__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1__Impl"


    // $ANTLR start "rule__State__Group__2"
    // InternalState1Dsl.g:181:1: rule__State__Group__2 : rule__State__Group__2__Impl ;
    public final void rule__State__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:185:1: ( rule__State__Group__2__Impl )
            // InternalState1Dsl.g:186:2: rule__State__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2"


    // $ANTLR start "rule__State__Group__2__Impl"
    // InternalState1Dsl.g:192:1: rule__State__Group__2__Impl : ( ( rule__State__TransitionsAssignment_2 )* ) ;
    public final void rule__State__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:196:1: ( ( ( rule__State__TransitionsAssignment_2 )* ) )
            // InternalState1Dsl.g:197:1: ( ( rule__State__TransitionsAssignment_2 )* )
            {
            // InternalState1Dsl.g:197:1: ( ( rule__State__TransitionsAssignment_2 )* )
            // InternalState1Dsl.g:198:2: ( rule__State__TransitionsAssignment_2 )*
            {
             before(grammarAccess.getStateAccess().getTransitionsAssignment_2()); 
            // InternalState1Dsl.g:199:2: ( rule__State__TransitionsAssignment_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalState1Dsl.g:199:3: rule__State__TransitionsAssignment_2
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__State__TransitionsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getStateAccess().getTransitionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // InternalState1Dsl.g:208:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:212:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // InternalState1Dsl.g:213:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // InternalState1Dsl.g:220:1: rule__Transition__Group__0__Impl : ( 'on' ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:224:1: ( ( 'on' ) )
            // InternalState1Dsl.g:225:1: ( 'on' )
            {
            // InternalState1Dsl.g:225:1: ( 'on' )
            // InternalState1Dsl.g:226:2: 'on'
            {
             before(grammarAccess.getTransitionAccess().getOnKeyword_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getOnKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // InternalState1Dsl.g:235:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:239:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // InternalState1Dsl.g:240:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // InternalState1Dsl.g:247:1: rule__Transition__Group__1__Impl : ( ( rule__Transition__EventAssignment_1 ) ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:251:1: ( ( ( rule__Transition__EventAssignment_1 ) ) )
            // InternalState1Dsl.g:252:1: ( ( rule__Transition__EventAssignment_1 ) )
            {
            // InternalState1Dsl.g:252:1: ( ( rule__Transition__EventAssignment_1 ) )
            // InternalState1Dsl.g:253:2: ( rule__Transition__EventAssignment_1 )
            {
             before(grammarAccess.getTransitionAccess().getEventAssignment_1()); 
            // InternalState1Dsl.g:254:2: ( rule__Transition__EventAssignment_1 )
            // InternalState1Dsl.g:254:3: rule__Transition__EventAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Transition__EventAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getEventAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // InternalState1Dsl.g:262:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:266:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // InternalState1Dsl.g:267:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // InternalState1Dsl.g:274:1: rule__Transition__Group__2__Impl : ( 'goto' ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:278:1: ( ( 'goto' ) )
            // InternalState1Dsl.g:279:1: ( 'goto' )
            {
            // InternalState1Dsl.g:279:1: ( 'goto' )
            // InternalState1Dsl.g:280:2: 'goto'
            {
             before(grammarAccess.getTransitionAccess().getGotoKeyword_2()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getGotoKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // InternalState1Dsl.g:289:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:293:1: ( rule__Transition__Group__3__Impl )
            // InternalState1Dsl.g:294:2: rule__Transition__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // InternalState1Dsl.g:300:1: rule__Transition__Group__3__Impl : ( ( rule__Transition__TargetAssignment_3 ) ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:304:1: ( ( ( rule__Transition__TargetAssignment_3 ) ) )
            // InternalState1Dsl.g:305:1: ( ( rule__Transition__TargetAssignment_3 ) )
            {
            // InternalState1Dsl.g:305:1: ( ( rule__Transition__TargetAssignment_3 ) )
            // InternalState1Dsl.g:306:2: ( rule__Transition__TargetAssignment_3 )
            {
             before(grammarAccess.getTransitionAccess().getTargetAssignment_3()); 
            // InternalState1Dsl.g:307:2: ( rule__Transition__TargetAssignment_3 )
            // InternalState1Dsl.g:307:3: rule__Transition__TargetAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Transition__TargetAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getTargetAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__StateMachine__StatesAssignment"
    // InternalState1Dsl.g:316:1: rule__StateMachine__StatesAssignment : ( ruleState ) ;
    public final void rule__StateMachine__StatesAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:320:1: ( ( ruleState ) )
            // InternalState1Dsl.g:321:2: ( ruleState )
            {
            // InternalState1Dsl.g:321:2: ( ruleState )
            // InternalState1Dsl.g:322:3: ruleState
            {
             before(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StateMachine__StatesAssignment"


    // $ANTLR start "rule__State__NameAssignment_1"
    // InternalState1Dsl.g:331:1: rule__State__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__State__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:335:1: ( ( RULE_ID ) )
            // InternalState1Dsl.g:336:2: ( RULE_ID )
            {
            // InternalState1Dsl.g:336:2: ( RULE_ID )
            // InternalState1Dsl.g:337:3: RULE_ID
            {
             before(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__NameAssignment_1"


    // $ANTLR start "rule__State__TransitionsAssignment_2"
    // InternalState1Dsl.g:346:1: rule__State__TransitionsAssignment_2 : ( ruleTransition ) ;
    public final void rule__State__TransitionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:350:1: ( ( ruleTransition ) )
            // InternalState1Dsl.g:351:2: ( ruleTransition )
            {
            // InternalState1Dsl.g:351:2: ( ruleTransition )
            // InternalState1Dsl.g:352:3: ruleTransition
            {
             before(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__TransitionsAssignment_2"


    // $ANTLR start "rule__Transition__EventAssignment_1"
    // InternalState1Dsl.g:361:1: rule__Transition__EventAssignment_1 : ( RULE_ID ) ;
    public final void rule__Transition__EventAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:365:1: ( ( RULE_ID ) )
            // InternalState1Dsl.g:366:2: ( RULE_ID )
            {
            // InternalState1Dsl.g:366:2: ( RULE_ID )
            // InternalState1Dsl.g:367:3: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__EventAssignment_1"


    // $ANTLR start "rule__Transition__TargetAssignment_3"
    // InternalState1Dsl.g:376:1: rule__Transition__TargetAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__TargetAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalState1Dsl.g:380:1: ( ( ( RULE_ID ) ) )
            // InternalState1Dsl.g:381:2: ( ( RULE_ID ) )
            {
            // InternalState1Dsl.g:381:2: ( ( RULE_ID ) )
            // InternalState1Dsl.g:382:3: ( RULE_ID )
            {
             before(grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0()); 
            // InternalState1Dsl.g:383:3: ( RULE_ID )
            // InternalState1Dsl.g:384:4: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getTargetStateIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__TargetAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});

}