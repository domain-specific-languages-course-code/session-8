/**
 * generated by Xtext 2.14.0
 */
package com.javadude.state1.ui.quickfix;

import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;

/**
 * Custom quickfixes.
 * 
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#quick-fixes
 */
@SuppressWarnings("all")
public class State1DslQuickfixProvider extends DefaultQuickfixProvider {
}
