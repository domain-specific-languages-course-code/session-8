package com.javadude.rube.state.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.javadude.rube.state.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'piece'", "'stuff'", "'that'", "'can'", "'happen'", "'show'", "'image'", "'for'", "'start'", "'here'", "'status'", "'when'", "'change'", "'to'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Piece";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRulePiece"
    // InternalMyDsl.g:64:1: entryRulePiece returns [EObject current=null] : iv_rulePiece= rulePiece EOF ;
    public final EObject entryRulePiece() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePiece = null;


        try {
            // InternalMyDsl.g:64:46: (iv_rulePiece= rulePiece EOF )
            // InternalMyDsl.g:65:2: iv_rulePiece= rulePiece EOF
            {
             newCompositeNode(grammarAccess.getPieceRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePiece=rulePiece();

            state._fsp--;

             current =iv_rulePiece; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePiece"


    // $ANTLR start "rulePiece"
    // InternalMyDsl.g:71:1: rulePiece returns [EObject current=null] : (otherlv_0= 'piece' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'stuff' otherlv_3= 'that' otherlv_4= 'can' otherlv_5= 'happen' ( (lv_stateMachine_6_0= ruleStateMachine ) ) ) ;
    public final EObject rulePiece() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_stateMachine_6_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( (otherlv_0= 'piece' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'stuff' otherlv_3= 'that' otherlv_4= 'can' otherlv_5= 'happen' ( (lv_stateMachine_6_0= ruleStateMachine ) ) ) )
            // InternalMyDsl.g:78:2: (otherlv_0= 'piece' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'stuff' otherlv_3= 'that' otherlv_4= 'can' otherlv_5= 'happen' ( (lv_stateMachine_6_0= ruleStateMachine ) ) )
            {
            // InternalMyDsl.g:78:2: (otherlv_0= 'piece' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'stuff' otherlv_3= 'that' otherlv_4= 'can' otherlv_5= 'happen' ( (lv_stateMachine_6_0= ruleStateMachine ) ) )
            // InternalMyDsl.g:79:3: otherlv_0= 'piece' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'stuff' otherlv_3= 'that' otherlv_4= 'can' otherlv_5= 'happen' ( (lv_stateMachine_6_0= ruleStateMachine ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPieceAccess().getPieceKeyword_0());
            		
            // InternalMyDsl.g:83:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalMyDsl.g:84:4: (lv_name_1_0= RULE_ID )
            {
            // InternalMyDsl.g:84:4: (lv_name_1_0= RULE_ID )
            // InternalMyDsl.g:85:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getPieceAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPieceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getPieceAccess().getStuffKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getPieceAccess().getThatKeyword_3());
            		
            otherlv_4=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_4, grammarAccess.getPieceAccess().getCanKeyword_4());
            		
            otherlv_5=(Token)match(input,15,FOLLOW_8); 

            			newLeafNode(otherlv_5, grammarAccess.getPieceAccess().getHappenKeyword_5());
            		
            // InternalMyDsl.g:117:3: ( (lv_stateMachine_6_0= ruleStateMachine ) )
            // InternalMyDsl.g:118:4: (lv_stateMachine_6_0= ruleStateMachine )
            {
            // InternalMyDsl.g:118:4: (lv_stateMachine_6_0= ruleStateMachine )
            // InternalMyDsl.g:119:5: lv_stateMachine_6_0= ruleStateMachine
            {

            					newCompositeNode(grammarAccess.getPieceAccess().getStateMachineStateMachineParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_2);
            lv_stateMachine_6_0=ruleStateMachine();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPieceRule());
            					}
            					set(
            						current,
            						"stateMachine",
            						lv_stateMachine_6_0,
            						"com.javadude.rube.state.MyDsl.StateMachine");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePiece"


    // $ANTLR start "entryRuleImage"
    // InternalMyDsl.g:140:1: entryRuleImage returns [EObject current=null] : iv_ruleImage= ruleImage EOF ;
    public final EObject entryRuleImage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImage = null;


        try {
            // InternalMyDsl.g:140:46: (iv_ruleImage= ruleImage EOF )
            // InternalMyDsl.g:141:2: iv_ruleImage= ruleImage EOF
            {
             newCompositeNode(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImage=ruleImage();

            state._fsp--;

             current =iv_ruleImage; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // InternalMyDsl.g:147:1: ruleImage returns [EObject current=null] : (otherlv_0= 'show' otherlv_1= 'image' ( (lv_file_2_0= RULE_STRING ) ) otherlv_3= 'for' ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleImage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_file_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalMyDsl.g:153:2: ( (otherlv_0= 'show' otherlv_1= 'image' ( (lv_file_2_0= RULE_STRING ) ) otherlv_3= 'for' ( (otherlv_4= RULE_ID ) ) ) )
            // InternalMyDsl.g:154:2: (otherlv_0= 'show' otherlv_1= 'image' ( (lv_file_2_0= RULE_STRING ) ) otherlv_3= 'for' ( (otherlv_4= RULE_ID ) ) )
            {
            // InternalMyDsl.g:154:2: (otherlv_0= 'show' otherlv_1= 'image' ( (lv_file_2_0= RULE_STRING ) ) otherlv_3= 'for' ( (otherlv_4= RULE_ID ) ) )
            // InternalMyDsl.g:155:3: otherlv_0= 'show' otherlv_1= 'image' ( (lv_file_2_0= RULE_STRING ) ) otherlv_3= 'for' ( (otherlv_4= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getImageAccess().getShowKeyword_0());
            		
            otherlv_1=(Token)match(input,17,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getImageAccess().getImageKeyword_1());
            		
            // InternalMyDsl.g:163:3: ( (lv_file_2_0= RULE_STRING ) )
            // InternalMyDsl.g:164:4: (lv_file_2_0= RULE_STRING )
            {
            // InternalMyDsl.g:164:4: (lv_file_2_0= RULE_STRING )
            // InternalMyDsl.g:165:5: lv_file_2_0= RULE_STRING
            {
            lv_file_2_0=(Token)match(input,RULE_STRING,FOLLOW_11); 

            					newLeafNode(lv_file_2_0, grammarAccess.getImageAccess().getFileSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImageRule());
            					}
            					setWithLastConsumed(
            						current,
            						"file",
            						lv_file_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_3=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getImageAccess().getForKeyword_3());
            		
            // InternalMyDsl.g:185:3: ( (otherlv_4= RULE_ID ) )
            // InternalMyDsl.g:186:4: (otherlv_4= RULE_ID )
            {
            // InternalMyDsl.g:186:4: (otherlv_4= RULE_ID )
            // InternalMyDsl.g:187:5: otherlv_4= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImageRule());
            					}
            				
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_4, grammarAccess.getImageAccess().getStateStateCrossReference_4_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleStateMachine"
    // InternalMyDsl.g:202:1: entryRuleStateMachine returns [EObject current=null] : iv_ruleStateMachine= ruleStateMachine EOF ;
    public final EObject entryRuleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateMachine = null;


        try {
            // InternalMyDsl.g:202:53: (iv_ruleStateMachine= ruleStateMachine EOF )
            // InternalMyDsl.g:203:2: iv_ruleStateMachine= ruleStateMachine EOF
            {
             newCompositeNode(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateMachine=ruleStateMachine();

            state._fsp--;

             current =iv_ruleStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalMyDsl.g:209:1: ruleStateMachine returns [EObject current=null] : ( ( ( (lv_images_0_0= ruleImage ) ) | ( (lv_states_1_0= ruleState ) ) )* otherlv_2= 'start' otherlv_3= 'here' ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_images_0_0 = null;

        EObject lv_states_1_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:215:2: ( ( ( ( (lv_images_0_0= ruleImage ) ) | ( (lv_states_1_0= ruleState ) ) )* otherlv_2= 'start' otherlv_3= 'here' ( (otherlv_4= RULE_ID ) ) ) )
            // InternalMyDsl.g:216:2: ( ( ( (lv_images_0_0= ruleImage ) ) | ( (lv_states_1_0= ruleState ) ) )* otherlv_2= 'start' otherlv_3= 'here' ( (otherlv_4= RULE_ID ) ) )
            {
            // InternalMyDsl.g:216:2: ( ( ( (lv_images_0_0= ruleImage ) ) | ( (lv_states_1_0= ruleState ) ) )* otherlv_2= 'start' otherlv_3= 'here' ( (otherlv_4= RULE_ID ) ) )
            // InternalMyDsl.g:217:3: ( ( (lv_images_0_0= ruleImage ) ) | ( (lv_states_1_0= ruleState ) ) )* otherlv_2= 'start' otherlv_3= 'here' ( (otherlv_4= RULE_ID ) )
            {
            // InternalMyDsl.g:217:3: ( ( (lv_images_0_0= ruleImage ) ) | ( (lv_states_1_0= ruleState ) ) )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==16) ) {
                    alt1=1;
                }
                else if ( (LA1_0==21) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:218:4: ( (lv_images_0_0= ruleImage ) )
            	    {
            	    // InternalMyDsl.g:218:4: ( (lv_images_0_0= ruleImage ) )
            	    // InternalMyDsl.g:219:5: (lv_images_0_0= ruleImage )
            	    {
            	    // InternalMyDsl.g:219:5: (lv_images_0_0= ruleImage )
            	    // InternalMyDsl.g:220:6: lv_images_0_0= ruleImage
            	    {

            	    						newCompositeNode(grammarAccess.getStateMachineAccess().getImagesImageParserRuleCall_0_0_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_images_0_0=ruleImage();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    						}
            	    						add(
            	    							current,
            	    							"images",
            	    							lv_images_0_0,
            	    							"com.javadude.rube.state.MyDsl.Image");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:238:4: ( (lv_states_1_0= ruleState ) )
            	    {
            	    // InternalMyDsl.g:238:4: ( (lv_states_1_0= ruleState ) )
            	    // InternalMyDsl.g:239:5: (lv_states_1_0= ruleState )
            	    {
            	    // InternalMyDsl.g:239:5: (lv_states_1_0= ruleState )
            	    // InternalMyDsl.g:240:6: lv_states_1_0= ruleState
            	    {

            	    						newCompositeNode(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_states_1_0=ruleState();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    						}
            	    						add(
            	    							current,
            	    							"states",
            	    							lv_states_1_0,
            	    							"com.javadude.rube.state.MyDsl.State");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_2=(Token)match(input,19,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getStateMachineAccess().getStartKeyword_1());
            		
            otherlv_3=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getStateMachineAccess().getHereKeyword_2());
            		
            // InternalMyDsl.g:266:3: ( (otherlv_4= RULE_ID ) )
            // InternalMyDsl.g:267:4: (otherlv_4= RULE_ID )
            {
            // InternalMyDsl.g:267:4: (otherlv_4= RULE_ID )
            // InternalMyDsl.g:268:5: otherlv_4= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateMachineRule());
            					}
            				
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_4, grammarAccess.getStateMachineAccess().getInitialStateStateCrossReference_3_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleState"
    // InternalMyDsl.g:283:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalMyDsl.g:283:46: (iv_ruleState= ruleState EOF )
            // InternalMyDsl.g:284:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalMyDsl.g:290:1: ruleState returns [EObject current=null] : (otherlv_0= 'status' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_transitions_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:296:2: ( (otherlv_0= 'status' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* ) )
            // InternalMyDsl.g:297:2: (otherlv_0= 'status' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* )
            {
            // InternalMyDsl.g:297:2: (otherlv_0= 'status' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* )
            // InternalMyDsl.g:298:3: otherlv_0= 'status' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )*
            {
            otherlv_0=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getStateAccess().getStatusKeyword_0());
            		
            // InternalMyDsl.g:302:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalMyDsl.g:303:4: (lv_name_1_0= RULE_ID )
            {
            // InternalMyDsl.g:303:4: (lv_name_1_0= RULE_ID )
            // InternalMyDsl.g:304:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_13); 

            					newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalMyDsl.g:320:3: ( (lv_transitions_2_0= ruleTransition ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==22) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMyDsl.g:321:4: (lv_transitions_2_0= ruleTransition )
            	    {
            	    // InternalMyDsl.g:321:4: (lv_transitions_2_0= ruleTransition )
            	    // InternalMyDsl.g:322:5: lv_transitions_2_0= ruleTransition
            	    {

            	    					newCompositeNode(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_13);
            	    lv_transitions_2_0=ruleTransition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transitions",
            	    						lv_transitions_2_0,
            	    						"com.javadude.rube.state.MyDsl.Transition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalMyDsl.g:343:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalMyDsl.g:343:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalMyDsl.g:344:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalMyDsl.g:350:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'when' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'change' otherlv_3= 'status' otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_event_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalMyDsl.g:356:2: ( (otherlv_0= 'when' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'change' otherlv_3= 'status' otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) ) )
            // InternalMyDsl.g:357:2: (otherlv_0= 'when' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'change' otherlv_3= 'status' otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) )
            {
            // InternalMyDsl.g:357:2: (otherlv_0= 'when' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'change' otherlv_3= 'status' otherlv_4= 'to' ( (otherlv_5= RULE_ID ) ) )
            // InternalMyDsl.g:358:3: otherlv_0= 'when' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'change' otherlv_3= 'status' otherlv_4= 'to' ( (otherlv_5= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getWhenKeyword_0());
            		
            // InternalMyDsl.g:362:3: ( (lv_event_1_0= RULE_ID ) )
            // InternalMyDsl.g:363:4: (lv_event_1_0= RULE_ID )
            {
            // InternalMyDsl.g:363:4: (lv_event_1_0= RULE_ID )
            // InternalMyDsl.g:364:5: lv_event_1_0= RULE_ID
            {
            lv_event_1_0=(Token)match(input,RULE_ID,FOLLOW_14); 

            					newLeafNode(lv_event_1_0, grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"event",
            						lv_event_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getChangeKeyword_2());
            		
            otherlv_3=(Token)match(input,21,FOLLOW_16); 

            			newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getStatusKeyword_3());
            		
            otherlv_4=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getToKeyword_4());
            		
            // InternalMyDsl.g:392:3: ( (otherlv_5= RULE_ID ) )
            // InternalMyDsl.g:393:4: (otherlv_5= RULE_ID )
            {
            // InternalMyDsl.g:393:4: (otherlv_5= RULE_ID )
            // InternalMyDsl.g:394:5: otherlv_5= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_5=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_5, grammarAccess.getTransitionAccess().getTargetStateCrossReference_5_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000290000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001000000L});

}