package com.javadude.state1.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.javadude.state1.services.State1DslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalState1DslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'state'", "'on'", "'goto'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalState1DslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalState1DslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalState1DslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalState1Dsl.g"; }



     	private State1DslGrammarAccess grammarAccess;

        public InternalState1DslParser(TokenStream input, State1DslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "StateMachine";
       	}

       	@Override
       	protected State1DslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStateMachine"
    // InternalState1Dsl.g:64:1: entryRuleStateMachine returns [EObject current=null] : iv_ruleStateMachine= ruleStateMachine EOF ;
    public final EObject entryRuleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateMachine = null;


        try {
            // InternalState1Dsl.g:64:53: (iv_ruleStateMachine= ruleStateMachine EOF )
            // InternalState1Dsl.g:65:2: iv_ruleStateMachine= ruleStateMachine EOF
            {
             newCompositeNode(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateMachine=ruleStateMachine();

            state._fsp--;

             current =iv_ruleStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalState1Dsl.g:71:1: ruleStateMachine returns [EObject current=null] : ( (lv_states_0_0= ruleState ) )* ;
    public final EObject ruleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject lv_states_0_0 = null;



        	enterRule();

        try {
            // InternalState1Dsl.g:77:2: ( ( (lv_states_0_0= ruleState ) )* )
            // InternalState1Dsl.g:78:2: ( (lv_states_0_0= ruleState ) )*
            {
            // InternalState1Dsl.g:78:2: ( (lv_states_0_0= ruleState ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalState1Dsl.g:79:3: (lv_states_0_0= ruleState )
            	    {
            	    // InternalState1Dsl.g:79:3: (lv_states_0_0= ruleState )
            	    // InternalState1Dsl.g:80:4: lv_states_0_0= ruleState
            	    {

            	    				newCompositeNode(grammarAccess.getStateMachineAccess().getStatesStateParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_states_0_0=ruleState();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    				}
            	    				add(
            	    					current,
            	    					"states",
            	    					lv_states_0_0,
            	    					"com.javadude.state1.State1Dsl.State");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleState"
    // InternalState1Dsl.g:100:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalState1Dsl.g:100:46: (iv_ruleState= ruleState EOF )
            // InternalState1Dsl.g:101:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalState1Dsl.g:107:1: ruleState returns [EObject current=null] : (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_transitions_2_0 = null;



        	enterRule();

        try {
            // InternalState1Dsl.g:113:2: ( (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* ) )
            // InternalState1Dsl.g:114:2: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* )
            {
            // InternalState1Dsl.g:114:2: (otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )* )
            // InternalState1Dsl.g:115:3: otherlv_0= 'state' ( (lv_name_1_0= RULE_ID ) ) ( (lv_transitions_2_0= ruleTransition ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getStateAccess().getStateKeyword_0());
            		
            // InternalState1Dsl.g:119:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalState1Dsl.g:120:4: (lv_name_1_0= RULE_ID )
            {
            // InternalState1Dsl.g:120:4: (lv_name_1_0= RULE_ID )
            // InternalState1Dsl.g:121:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalState1Dsl.g:137:3: ( (lv_transitions_2_0= ruleTransition ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalState1Dsl.g:138:4: (lv_transitions_2_0= ruleTransition )
            	    {
            	    // InternalState1Dsl.g:138:4: (lv_transitions_2_0= ruleTransition )
            	    // InternalState1Dsl.g:139:5: lv_transitions_2_0= ruleTransition
            	    {

            	    					newCompositeNode(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_transitions_2_0=ruleTransition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transitions",
            	    						lv_transitions_2_0,
            	    						"com.javadude.state1.State1Dsl.Transition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalState1Dsl.g:160:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalState1Dsl.g:160:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalState1Dsl.g:161:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalState1Dsl.g:167:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'on' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'goto' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_event_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalState1Dsl.g:173:2: ( (otherlv_0= 'on' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'goto' ( (otherlv_3= RULE_ID ) ) ) )
            // InternalState1Dsl.g:174:2: (otherlv_0= 'on' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'goto' ( (otherlv_3= RULE_ID ) ) )
            {
            // InternalState1Dsl.g:174:2: (otherlv_0= 'on' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'goto' ( (otherlv_3= RULE_ID ) ) )
            // InternalState1Dsl.g:175:3: otherlv_0= 'on' ( (lv_event_1_0= RULE_ID ) ) otherlv_2= 'goto' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getOnKeyword_0());
            		
            // InternalState1Dsl.g:179:3: ( (lv_event_1_0= RULE_ID ) )
            // InternalState1Dsl.g:180:4: (lv_event_1_0= RULE_ID )
            {
            // InternalState1Dsl.g:180:4: (lv_event_1_0= RULE_ID )
            // InternalState1Dsl.g:181:5: lv_event_1_0= RULE_ID
            {
            lv_event_1_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_event_1_0, grammarAccess.getTransitionAccess().getEventIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"event",
            						lv_event_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getGotoKeyword_2());
            		
            // InternalState1Dsl.g:201:3: ( (otherlv_3= RULE_ID ) )
            // InternalState1Dsl.g:202:4: (otherlv_3= RULE_ID )
            {
            // InternalState1Dsl.g:202:4: (otherlv_3= RULE_ID )
            // InternalState1Dsl.g:203:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getTargetStateCrossReference_3_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});

}